# Scanpy helper routines

# load loomfile
import os
from collections import OrderedDict
import pandas as pd
import loompy as lp
from path import Path
import scanpy.api as sc
import tempfile
import numpy as np
import logging

from path import Path


## load genesets
def load_gmt(name, filename):
    rv = {}
    with open(filename) as F:
        for line in F:
            ls = line.split("\t")
            gsname = '{}_{}'.format(name, ls[0])
            rv[gsname] = ls[2:]
    return rv

def load_genesets(path):
    genesets = {}
    for gsdir in Path(path).dirs():
        geneset = gsdir.basename()
        for gsfile in gsdir.files():
            gsname = gsfile.basename().replace('.grp', '')
            genes = open(gsfile).read().split()
            genesets['{}__{}'.format(geneset, gsname)] = genes
    logging.info("loaded {} geneset".format(len(genesets)))
    return genesets

def metadata_info(m):
    rn = {}; rc = {}
    for k, v in m.iteritems():
        if np.issubdtype(v.dtype, np.number):
            rn[k] = dict(
                min=v.min(), max=v.max(),
                mean=v.mean(), median=v.median())
        else:
            vv = v.value_counts().sort_values(ascending=False)
            rc[k] = {
                'no unique': len(vv),
                'most abundant': '"' + '", "'.join(vv.index[:3]) + '"',
            }

    rn = pd.DataFrame(rn).T
    rn = rn['min max mean median'.split()]
    return rn, rc


def loomload(loomfile, copy=True):

    loomfile = Path(loomfile)

    if copy:
        # first copy file to other location
        tmpdir = Path(tempfile.mkdtemp(suffix='rat'))
        tmploom = tmpdir / loomfile.basename()
        loomfile.copy(tmploom)
        loomfile = tmploom

    with lp.connect(loomfile) as loom:
        print('opened loom', loomfile)
        print('shape', loom.shape)
        rv = sc.AnnData(X=loom[:,:].T)
        rv.var_names = loom.ra['Gene']
        rv.row_names = loom.ca['CellID']
        obs = {}
        for k in loom.ca:
            if k in 'CellID'.split(): continue
            if k.startswith('Embedd'): continue
            if k.startswith('Reg'): continue
            rv.obs[k] = loom.ca[k]

        rv.obsm['X_tsne'] = np.array(list(map(tuple, loom.ca['Embedding'])))
        return rv
